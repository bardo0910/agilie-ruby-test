# Pack Handler
---
Создайте класс обработчика, который примет массив элементов и выполнит действие для каждого элемента, но только если элемент еще не обработан. Идея заключается в том, что этот класс будет полезен для обработки нескольких партий элементов, гарантируя, что каждый элемент обрабатывается один раз, даже если элемент включен в несколько партий.

## proc_items
##### Вызывается с массивом в качестве аргумента и требует блока. Передаваемый блок используется для обработки элемента, который будет вызываться только в том случае, если определяет, что элемент должен быть обработан. 
 Блок передаваемый методу должен вернуть обработанный элемент.

```ruby
pack_handler = PackHandler.new
pack_handler.proc_items([1,2,3,4]) do |item|
 item * 10
end
```
С хешами:
```ruby
pack_handler = PackHandler.new
pack_handler.identify(:id)
input = [ { id: 1, value: 'test'}, {id: 2, value: 'test'}, {id: 3, value: 'test'}]
pack_handler.proc_items(input) do |item|
 item[:value] = 'new_value'
 item
end
```
С объектами:
```ruby
class SomeClass
 attr_accessor :main_field
 def initialize(main_field)
   @main_field = main_field
 end
end

a = SomeClass.new('a')
b = SomeClass.new('b')

pack_handler = PackHandler.new
pack_handler.identify(:main_field)
pack_handler.proc_items([a, b]) do |item|
 item.main_field = 'new_value'
 item
end
```

## procd_items
##### Возвращает массив обработанных элементов.
```ruby
pack_handler = PackHandler.new
pack_handler.proc_items([1,2,3,4]) do |item|
 item * 10
end

pack_handler.procd_items # returns Array [10,20,30,40]
```
## idefine

##### Определеляет каким образом разные экземпляры объектов  будут считаться одинаковыми. Если он вызван, он определит, какой хэш-ключ использовать в качестве критерия сравнения (если переданы хеши) или какой метод использовать (если предоставляется объект). Он принимает символ или строковый аргумент, который определяет имя хеш-ключа / метода, которое будет использоваться для идентификации элемента. Если этот метод не вызывается, экземпляры будут сравниваться друг с другом обычным образом.

## should_proc

##### Цель этого метода - настроить передачу необязательного блока, который будет использоваться для определения того, должен ли элемент обрабатываться. Этот необязательный блок возвращает true, если элемент должен быть обработан, иначе он вернет false. Когда этот метод используется, блок, переданный ему, будет использоваться совместно с логикой «этот элемент уже обработан?»

```ruby
pack_handler.identify(:value)
pack_handler.should_proc do |item|
 item[:value] % 2 == 0
end

pack_handler.proc_items([{value: 2}, {value: 3}]) do |item|
 item[:is_checked] = true
 item
end

pack_handler.proc_items([{value: 2}, {value: 6}]) do |item|
 item[:is_checked] = true
 item
end

pack_handler.procd_items # returns array [ { value: 2, is_checked: true}, { value: 6, is_checked: true} ]
```

## reset

##### Цель этого метода - сбросить состояние обработанных элементов, позволяя повторно обрабатывать элементы.


