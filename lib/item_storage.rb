class ItemStorage

  attr_reader :processed_items, :input_items

  def initialize
    @processed_items = []
    @input_items = []
  end

  def add_item(input_item, processed_item)
    @input_items << input_item
    @processed_items << processed_item
  end

  def reset
    @processed_items = []
    @input_items = []
  end



end