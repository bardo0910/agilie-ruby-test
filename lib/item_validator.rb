class ItemValidator

  attr_accessor :items_array,
                :item,
                :identificator,
                :validation_proc

  def initialize(items_array, item, identificator, validation_proc)
    @items_array = items_array
    @item = item
    @identificator = identificator
    @validation_proc = validation_proc
  end

  def validate
    result = @identificator ? valid_by_identificator? : valid_default?
    result && @validation_proc ? valid_by_proc? : result
  end

  protected


  def valid_by_identificator?
    @items_array.none? do |item|
      if item.is_a?(Hash)
        item[@identificator].eql?(@item[@identificator])
      else
        item.send(@identificator).eql?(@item.send(@identificator))
      end
    end
  end

  def valid_default?
    !@items_array.include?(@item)
  end

  def valid_by_proc?
    @validation_proc.call(@item)
  end
end