require_relative 'item_storage'
require_relative 'item_validator'

class PackHandler

  def initialize
    @storage = ItemStorage.new
    @identificator = nil
    @validation_proc = nil
  end

  def procd_items
    @storage.processed_items
  end

  def should_proc(&proc)
    @validation_proc = proc
  end

  def proc_items(items)
    raise 'Input parameter must be an array type' unless items.is_a?(Array)

    items.each do |item|
      next if !item_should_processed?(item) || !block_given?
      input_item = item.clone
      processed_item = yield(item)
      @storage.add_item(input_item, processed_item)
    end
  end

  def identify(identificator)
    @identificator = identificator
  end

  def reset
    @storage.reset
  end

  protected

  def item_should_processed?(item)
    validator = ItemValidator.new(
                                 @storage.input_items,
                                 item,
                                 @identificator,
                                 @validation_proc
    )

    validator.validate

  end

end