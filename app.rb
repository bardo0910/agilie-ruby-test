require_relative 'lib/pack_handler'

pack_handler = PackHandler.new

pack_handler.identify(:value)
pack_handler.should_proc do |item|
 item[:value] % 2 == 0
end

pack_handler.proc_items([{value: 2}, {value: 3}]) do |item|
 item[:is_checked] = true
 item
end

pack_handler.proc_items([{value: 2}, {value: 6}]) do |item|
 item[:is_checked] = true
 item
end

puts pack_handler.procd_items
